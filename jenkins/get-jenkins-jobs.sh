#!/bin/bash

JENKINS_USERNAME=$1
JENKINS_PASSWORD=$2

LOGIN_CREDENTIALS="--user $JENKINS_USERNAME:$JENKINS_PASSWORD"

curl -X GET http://sabingautam-1904517299.us-west-2.elb.amazonaws.com/post/jenkins/job/clean-docker-image-and-resources/config.xml $LOGIN_CREDENTIALS -o clean-docker-image-and-resources.xml

curl -X GET http://sabingautam-1904517299.us-west-2.elb.amazonaws.com/post/jenkins/job/greenhouse-webapp-build/config.xml $LOGIN_CREDENTIALS -o greenhouse-webapp-build.xml

curl -X GET http://sabingautam-1904517299.us-west-2.elb.amazonaws.com/post/jenkins/job/tomcat-docker-container-build/config.xml $LOGIN_CREDENTIALS -o tomcat-docker-container-build.xml

curl -X GET http://sabingautam-1904517299.us-west-2.elb.amazonaws.com/post/jenkins/job/build-and-ship-docker-with-greenhouse-war/config.xml $LOGIN_CREDENTIALS -o build-and-ship-docker-with-greenhouse-war.xml
