<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="intro">
	<h2>Welcome to Sabin's Customized Greenhouse!</h2>
	<p>
		We make it fun to be an application developer.
	</p>
	<p>
		We help you connect with fellow developers and take advantage of everything the Spring community has to offer.	
	</p>
	<p>
	    <ul>
            <li><a href="<c:url value="/signin" />">Sign In</a></li>
            <li><a href="<c:url value="/signup" />">Join Now</a></li>
            <li><a href="<c:url value="http://sabingautam-1904517299.us-west-2.elb.amazonaws.com" />">SEIS 708 Main</a></li>
        </ul>
    <p>
</div>