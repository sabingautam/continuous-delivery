#!/bin/bash

set +xe

WAR_FILE_NAME=$1
REPO_NAME=$2
SOURCE_REPO_CONTAINER_NAME=$3
SOURCE_REPO_TAG=$4

DOCKER_IMAGE=$REPO_NAME/$SOURCE_REPO_CONTAINER_NAME:$SOURCE_REPO_TAG
DEPLOYABLE_WAR_FILE_NAME=post.war

main() {
    getDeployableWAR
    configureDockerfile
    copyWARAndStartTomcat
}

getDeployableWAR() {
    aws s3 cp s3://sabingautam/continuous-integration/war/v1/$WAR_FILE_NAME $DEPLOYABLE_WAR_FILE_NAME
}

configureDockerfile() {
    sed -i -e "s/@REPO_NAME@/${REPO_NAME}/g" Dockerfile
    sed -i -e "s/@REPO_CONTAINER_NAME@/${SOURCE_REPO_CONTAINER_NAME}/g" Dockerfile
    sed -i -e "s/@REPO_TAG@/${SOURCE_REPO_TAG}/g" Dockerfile
    sed -i -e "s/@WAR_FILENAME@/${DEPLOYABLE_WAR_FILE_NAME}/g" Dockerfile
}

copyWARAndStartTomcat() {
    echo "Building new docker image with the updated war."
    sudo docker build -t $DOCKER_IMAGE .

    echo "Stopping running docker containers"
    sudo docker rm -fv $(sudo docker ps -q)

    echo "Starting newly updated docker container."
    sudo docker run -idt -p 8080:8080 gautamsabin/greenhouse:latest
}

printUsage() {
    echo -e "\n"
    echo "Usage: $0
          arg1 = War filename.
          arg2 = Docker repo name.
          arg3 = Docker repo container name.
          arg4 = Docker Container tag."
    echo -e "\n"
    exit 1
}

if [ $# -lt 4 ];then
    printUsage
    exit 1
fi

main

